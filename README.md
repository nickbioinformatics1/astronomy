# Transient Processing

### Summary
This repo contains a jupyter notebook to process astronomical images to detect transient events (I am looking for exoplanets) for stars in an image. This was developed to learn about the basics of astronomy image processing, computer vision, and practice with leveraging python classes.


### About the Data
<details><summary>Image acquisition</summary>
The images used for this project were taken using Celestron edgeHD 9.25 with a 0.7x reducer. The imaging camera is a QHY268M camera and the images were taken in grayscale.
</details>

<details><summary>Data Preprocessing </summary>
To account for discrepencies between each image, the stack of images were aligned using `AstroImageJ`. These aligned and adjusted images were used as input for python jupyter notebook.
</details>

### Processing Steps
* Each fits image is read in using  `astropy.io`
* The contrast of each image is adjusted using `cv2`
* Images are viewed using `matplotlib`
* To detect stars in an image, two methods are available within the python class:
    * Gaussian Blur
    * Local Peak Maximum
* The coordinates of each star is used to measure the intensity of the star within the time-series set of images
* For each detected star, a plot is produced containing the location of the star along with a time-series line plot containing the pixel intensity of that star.

### Improvements  
* Integrate DaostarFinder
* Build CNN model for star detection in an image
* Improve plots / create Data visualization app


